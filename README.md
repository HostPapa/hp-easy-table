# HostPapa EasyTable #

This plugin is a self-maintained version of the discontinued _EasyTable_ plugin. We forked the code and addressed the reported security issue.

## Prerequisites ##

WordPress += 4.x

## Installing ##

Add the following to `composer.json`:

```json
repositories
{
    "type": "vcs",
    "url": "https://bitbucket.org/HostPapa/hp-easy-table"
},
require {
    "hostpapa/wp-hostpapa-shortcodes": "^1.9"
}
```

To install, run:

```
$ composer install
```

## Usage ##

This plugin works in the same way its predecessor did. Refer to EasyTable documentation for further help.

## Versioning ##

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/HostPapa/wp-hostpapa-shortcodes/downloads/?tab=tags).

## Authors ##

-   **Pavel Espinal** - *Plugin import and patching, composer package*
